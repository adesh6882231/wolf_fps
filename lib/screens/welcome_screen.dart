import 'package:flutter/material.dart';
import 'package:wolf_fps/widgets/button.dart';

class WelcomScreen extends StatelessWidget {
  const WelcomScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: customButton(
            onPressed: () {
              Navigator.of(context).popAndPushNamed('/game');
            },
            child: const Text('Play'),
          ),
        ),
      ),
    );
  }
}
