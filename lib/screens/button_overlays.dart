import 'package:flutter/material.dart';

class ButtonOverlays extends StatefulWidget {
  const ButtonOverlays({super.key});

  @override
  State<ButtonOverlays> createState() => _ButtonStates();
}

class _ButtonStates extends State<ButtonOverlays> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).popAndPushNamed('/');
          },
          child: const Text('Go back')),
    );
  }
}
