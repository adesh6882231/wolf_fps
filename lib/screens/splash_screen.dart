import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.delayed(const Duration(seconds: 2)).then((value) {
          return Navigator.of(context).popAndPushNamed('/welcome');
        }),
        builder: (context, asyncSnapshot) {
          return Container(
            color: Colors.white,
            child: const SizedBox(
              height: 50,
              width: 50,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        });
  }
}
