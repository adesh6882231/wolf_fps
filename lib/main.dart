import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flame/game.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:wolf_fps/game.dart';
import 'package:wolf_fps/config/app_config.dart';
import 'package:wolf_fps/firebase/firebase_options.dart';
import 'package:wolf_fps/screens/button_overlays.dart';
import 'package:wolf_fps/screens/splash_screen.dart';
import 'package:wolf_fps/screens/welcome_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  // Pass all uncaught "fatal" errors from the framework to Crashlytics
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;

  // initialize game
  FlameGame game = AppConfig().init();

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const SplashScreen(),
        '/welcome': (context) => const WelcomScreen(),
        '/game': (context) => GameWidget(
              game: kDebugMode ? WOLF() : game,
              overlayBuilderMap: {
                'PauseMenu': (BuildContext context, Game game) {
                  return const ButtonOverlays();
                },
              },
            ),
      },
    ),
  );
}
