import 'dart:async';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:wolf_fps/entities/animated/player.dart';
import 'package:wolf_fps/levels/level_01.dart';
import 'package:wolf_fps/state_manager/state_manager.dart';

class WOLF extends FlameGame {
  final currentState = StateManager();
  final currentPlayer = Player();
  late final CameraComponent cam;
  final currentWorld = Level01();
  final pauseOverlayIdentifier = 'PauseMenu';

  @override
  FutureOr<void> onLoad() {
    cam = CameraComponent.withFixedResolution(
        world: currentWorld, width: 7650, height: 5100);
    cam.viewfinder.anchor = Anchor.topLeft;
    addAll([cam, currentWorld]);
    overlays.add(pauseOverlayIdentifier);
    return super.onLoad();
  }
}
