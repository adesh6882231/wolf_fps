import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:wolf_fps/game.dart';

class AppConfig {
  AppConfig({game});
  FlameGame init() {
    Flame.device.fullScreen();
    Flame.device.setLandscape();
    return WOLF();
  }
}
