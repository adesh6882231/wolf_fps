import 'package:flutter/material.dart';

ElevatedButton customButton(
    {required void Function()? onPressed, required Widget child}) {
  return (ElevatedButton(
    onPressed: onPressed,
    child: child,
  ));
}
